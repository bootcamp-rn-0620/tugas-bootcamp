// Soal 1
// Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah 
// array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter 
// pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).

// struktur fungsinya seperti berikut range(startNum, finishNum) {} 
// Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1

function range(startNum, finishNum) {
  var rangeArr = [];
  // var rangeLength = Math.abs(startNum - finishNum) + 1;

  if (startNum > finishNum) {
    var rangeLength = startNum - finishNum + 1;
    for (var i = 0; i < rangeLength; i++) {
      rangeArr.push(startNum - i)
    }
  } else if (startNum < finishNum) {
    var rangeLength = finishNum - startNum + 1;
    for (var i = 0; i < rangeLength; i++) {
      rangeArr.push(startNum + i)
    }
  } else if (!startNum || !finishNum) {
    return -1
  }
  return rangeArr
}

console.log("=================Soal 1=================")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal 2

// Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya 
// namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih 
// atau beda dari setiap angka pada array. 
// Jika parameter pertama lebih besar dibandingkan parameter kedua 
// maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.

// struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}

function rangeWithStep(startNum, finishNum, step) {
  var rangeArr = [];

  if (startNum > finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum >= finishNum; i++) {
      rangeArr.push(currentNum)
      currentNum -= step
    }
  } else if (startNum < finishNum) {// start = 1; finish =10; step=2
    var currentNum = startNum;
    for (var i = 0; currentNum <= finishNum; i++) { // i =1
      rangeArr.push(currentNum) //3
      currentNum += step // 9 + 2 = 11
    }
  } else if (!startNum || !finishNum || !step) {
    return -1
  }
  return rangeArr
}

console.log("=================Soal 2=================")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3

// Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, 
// angka akhir deret, dan beda jarak (step).  
// Function akan mengembalikan nilai jumlah (sum) dari deret angka. 
// contohnya sum(1,10,1) akan menghasilkan nilai 55. 

// ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.

// Code di sini

function sum(startNum, finishNum, step) {
  var rangeArr = [];
  var distance;

  if (!step) {
    distance = 1
  } else {
    distance = step
  }

  if (startNum > finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum >= finishNum; i++) {
      rangeArr.push(currentNum)
      currentNum -= distance
    }
  } else if (startNum < finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum <= finishNum; i++) {
      rangeArr.push(currentNum)
      currentNum += distance
    }
  } else if (!startNum && !finishNum && !step) {
    return 0
  } else if (startNum) {
    return startNum
  }

  var total = 0;
  for (var i = 0; i < rangeArr.length; i++) {
    total = total + rangeArr[i]
  }
  return total
}

console.log("=================Soal 3=================")
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal 4

// Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. 
// Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n. 
// Contoh input dapat dilihat dibawah:

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], // 0
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], // 1
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], // 2
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] // 3
];

function dataHandling(data) {
  var dataLength = data.length
  for (var i = 0; i < dataLength; i++) {
    var id = 'Nomor ID: ' + data[i][0]
    var nama = 'Nama Lengkap: ' + data[i][1]
    var ttl = 'TTL: ' + data[i][2] + ' ' + data[i][3]
    var hobi = 'Hobi: ' + data[i][4]
    console.log(id)
    console.log(nama)
    console.log(ttl)
    console.log(hobi)
    console.log()
  }
}

// panggil fungsi:
console.log("=================Soal 4=================")
dataHandling(input);

// Nomor ID:  0001
// Nama Lengkap:  Roman Alamsyah
// TTL:  Bandar Lampung 21/05/1989
// Hobi:  Membaca

// Nomor ID:  0002
// Nama Lengkap:  Dika Sembiring
// TTL:  Medan 10/10/1992
// Hobi:  Bermain Gitar

// Nomor ID:  0003
// Nama Lengkap:  Winona
// TTL:  Ambon 25/12/1965
// Hobi:  Memasak

// Nomor ID:  0004
// Nama Lengkap:  Bintang Senjaya
// TTL:  Martapura 6/4/1970
// Hobi:  Berkebun 

// ! Coretan Live Session
// function loopMulti(data) {
//   var newArr1 = []
//   // for (var i = 0; i < data.length; i++) {
//   //   // console.log(data[i])
//   //   var newArr2 = []
//   //   for (var j = 0; j < data[i].length; j++) {
//   //     // console.log(data[i][j])
//   //     newArr2.push(data[i][j])
//   //   }
//   //   newArr1.push(newArr2)
//   // }
//   // data = []
//   // console.log(data)
// }

// function tambah(a, b) {
//   var c = a + b
//   // console.log(c)
//   // return c
//   function panggil(c) {
//     console.log(c)
//   }
//   panggil(c)
// }
// tambah(1, 2)
// panggil(tambah(1, 2))
// loopMulti(input)

// Soal 5

// Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string 
// dan mengembalikan kebalikan dari string tersebut. 

// Code di sini
function balikKata(kata) {
  // var oldKata = kata
  var newKata = '';
  for (var i = kata.length - 1; i >= 0; i--) { // length =11; index = 10
    newKata += kata[i]
  }
  return newKata;
}

console.log("=================Soal 5=================")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6

//contoh output
// ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  

// Buatlah sebuah function dengan nama dataHandling2 yang akan menerima input array seperti di atas.
// Gunakan fungsi splice untuk memodifikasi variabel tersebut agar menjadi seperti array dibawah. 
// Lalu console.log array yang baru seperti di bawah. 

// ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 

// Berdasarkan elemen yang berisikan tanggal/bulan/tahun (elemen ke-4), ambil angka bulan 
//  dan console.log nama bulan sesuai dengan angka tersebut.
// Gunakan split untuk memisahkan antara tanggal, bulan, dan tahun.
// Format tanggal pada data adalah dd-mm-YYYY
// Misal angka bulan 01, tuliskan "Januari"
// Gunakan switch case untuk menuliskan bulan di atas.
// Pada array hasil split dari tanggal/bulan/tahun, lakukan sorting secara descending 
//  dan console.log array yang sudah di-sort.
// Masih pada array hasil split dari elemen tanggal/bulan/tahun, 
//  gabungkan semua elemen menggunakan join dan pisahkan dengan karakter strip (-) lalu console.log hasilnya.
// Nama (elemen ke-2), harus dibatasi sebanyak 15 karakter saja. 
// Gunakan slice untuk menghapus kelebihan karakter dan console.log nama yang sudah di-slice, 
// sebelum di-slice pastikan Nama (elemen ke-2) sudah dalam bentuk String agar bisa di-slice.


console.log("=================Soal 6=================")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data) {
  var newData = data

  var newName = data[1] + 'Elsharawy' // Roman Alamsyah Elsharawy
  var newProvince = 'Provinsi ' + data[2] // Provinsi Bandar Lampung
  var gender = 'Pria'
  var institution = 'SMA Internasional Metro'

  newData.splice(1, 1, newName)
  newData.splice(2, 1, newProvince)
  newData.splice(4, 1, gender, institution)

  var arrDate = data[3]
  var newDate = arrDate.split('/') // ['21', '05', '1989' ]
  var monthNum = newDate[1]
  var monthName = ''

  switch (monthNum) {
    case '01':
      monthName = 'Januari';
      break;
    case '02':
      monthName = 'Februari';
      break;
    case '03':
      monthName = 'Maret';
      break;
    case '04':
      monthName = 'April';
      break;
    case '05':
      monthName = 'Mei';
      break;
    case '06':
      monthName = 'Juni';
      break;
    case '07':
      monthName = 'Juli';
      break;
    case '08':
      monthName = 'Agustus';
      break;
    case '09':
      monthName = 'September';
      break;
    case '10':
      monthName = 'Oktober';
      break;
    case '11':
      monthName = 'November';
      break;
    case '12':
      monthName = 'Desember';
      break;
    default:
      break;
  }

  var dateJoin = newDate.join('-')
  var dateArr = newDate.sort(function (value1, value2) { return value2 - value1 }) //desc
  var editName = newName.slice(0, 15)

  console.log(newData) // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
  console.log(monthName) // Mei
  console.log(dateArr) // ['1989', '21', '05']
  console.log(dateJoin) // 21-05-1989
  console.log(editName) //Roman Alamsyah
}

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
