// Ascending Ten (10 poin)

// Buatlah sebuah function dengan nama AscendingTen yang menerima sebuah parameter berupa Number, 
// function AscendingTen tersebut akan mengembalikan deretan angka yang ditampilkan 
// dalam satu baris (ke samping). Deret angka yang ditampilkan adalah deretan angka 
// mulai dari angka yang menjadi parameter input function hingga 10 angka setelahnya yang dipisah dengan karakter spasi. 
// Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.

// contoh: 
// console.log(AscendingTen(1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10  
// console.log(Ascending(101)) akan menampilkan 101 102 103 104 105 106 107 108 109 110

// Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.

function AscendingTen(num) {
  // Tulis code kamu di sini
  if (!num) {
    return -1
  }

  var number = num.toString(); // 11
  for (var i = 1; i <= 9; i++) { // i = 2
    var add = num + i; // 11 + 2 = 13
    number = number + ' ' + add.toString()  // 11 12 13 
  }
  return number
}

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

