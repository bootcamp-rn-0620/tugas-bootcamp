// Balik String (10 poin)

// Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. 
// Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. 
// contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

// NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
// Hanya boleh gunakan looping. 

function balikString(kuta) {
    // function balikString(kata) {
  // var oldKata = kata;
  var panjangKata = kata.length;
  var newKata = "";
  for (var i = panjangKata - 1; i >= 0; i--){
    newKata += kata[i]
  }
  return  newKata
  // Silakan tulis code kamu di sini
}

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
