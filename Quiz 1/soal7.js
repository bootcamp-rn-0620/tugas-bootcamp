// Papan Ular Tangga (35)
// Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil 
// akan menampilkan papan ular tangga ukuran 10 x 10. 

// Output: 
// 100 99 98 97 96 95 94 93 92 91 // baris ke 1
// 81 82 83 84 85 86 87 88 89 90 // baris ke 2
// 80 79 78 77 76 75 74 73 72 71 // baris ke 3
// 61 62 63 64 65 66 67 68 69 70 // baris ke 4
// 60 59 58 57 56 55 54 53 52 51
// 41 42 43 44 45 46 47 48 49 50
// 40 39 38 37 36 35 34 33 32 31
// 21 22 23 24 25 26 27 28 29 30
// 20 19 18 17 16 15 14 13 12 11
// 1 2 3 4 5 6 7 8 9 10

function ularTangga() {
  // Tulis code kamu di sini
  // var panjang = 10;
  // var lebar = 10;
  // var totalPapan = panjang * lebar;

  var totalPapan = 100;

  var reference = totalPapan; // yang akan dijadikan reference pada fungsi Asc dan Dsc
  var number = reference.toString(); // yang akan ditampilkan di log;

  for (var j = 1; j <= 10; j++) {
    if (j % 2 == 1) {
      for (var i = 1; i <= 9; i++) {
        reference = reference - 1;
        number = number + ' ' + reference.toString()
      }
    } else if (j % 2 == 0) {
      for (i = 1; i <= 9; i++) {
        reference = reference + 1;
        number = number + ' ' + reference.toString()
      }
    }

    if (j != 10) {
      number += '\n'
      reference -= 10
      number += ' ' + reference.toString()
    }
  }


  // //mulai dari 100 ke 91
  // for (var i = 1; i <= 9; i++) {
  //   reference = reference - 1;
  //   number = number + ' ' + reference.toString()
  
  // }

  // number += '\n'
  // reference -= 10;
  // number += ' ' + reference.toString()

  // // dari 81 ke 90
  // for (i = 1; i <= 9; i++) {
  //   reference = reference + 1;
  //   number = number + ' ' + reference.toString()
  // }

  // number += '\n'
  // reference -= 10
  // number += ' ' + reference.toString()

  // // dari 80 - 71
  // for (var i = 1; i <= 9; i++) {
  //   reference = reference - 1;
  //   number = number + ' ' + reference.toString()
  // }

  // number += '\n'
  // reference -= 10;
  // number += ' ' + reference.toString()

  // // dari 61 - 70
  // for (i = 1; i <= 9; i++) {
  //   reference = reference + 1;
  //   number = number + ' ' + reference.toString()
  // }

  // number += '\n'
  // reference -= 10;
  // number += ' ' + reference.toString()

  return number
}

// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/



// function ularTangga() {
//   var hs="";
//   var n=100;
//   for (var i=1; i<=10; i++) {
//     if (i % 2 == 1) {
//       hs=hs+DescendingTen(n)+"\n";
//       n=n-20;
//     } else {
//       hs=hs+AscendingTen(n+1)+"\n";
//     }
//   }
//   return hs;
// }

// function ularTangga() {
//   // Tulis code kamu di sini
//   var result = ""
//   for(var i = 10; i >= 1; i--){
//     for(var j = 10; j >= 1; j--){
//       if(i%2 == 0){
//         result += ((i-(i%2))*10)-(10-j) + " "
//       }else{
//         result += ((i-(i%2))*10)+(10-j+1) + " "
//       }
      
//     }
//     result += '\n'
//   }
//   return result
// }