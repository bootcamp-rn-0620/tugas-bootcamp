// Descending Ten (10 poin)

// Function DescendingTen adalah kebalikan dari function AscendingTen. 
// Output yang diharapkan adalah deretan angka dimulai dari angka parameter hingga 10 angka di bawahnya. 
// Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.

// contoh: 
// console.log(DescendingTen(10)) akan menampilkan 10 9 8 7 6 5 4 3 2 1
// console.log(DescendingTen(20)) akan menampilkan 20 19 18 17 16 15 14 13 12 11

// Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.

function DescendingTen(num) {
  // Tulis code kamu di sini
  if (!num) {
    return -1
  }

  var number = num.toString(); // 100
  for (var i = 1; i <= 9; i++) { // i = 1
    var add = num - i; // 100 - 1 = 99
    number = number + ' ' + add.toString() // 100 99
  }

  return number
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
