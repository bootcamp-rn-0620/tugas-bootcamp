// Palindrome (10 poin)

// Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
// Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
// Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
// Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

// NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
// Hanya boleh gunakan looping. 

function palindrome(kata) {
  // Silakan tulis code kamu di sini
  var oldKata = kata;
  var panjangKata = kata.length;
  var newKata = "";
  for (var i = panjangKata - 1; i >= 0; i--) {
    newKata += kata[i]
  }
  return  oldKata == newKata
  // Silakan tulis code kamu di sini
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false