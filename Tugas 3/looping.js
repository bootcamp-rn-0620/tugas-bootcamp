
//? Soal 1 - while

//? Output:
//? LOOPING PERTAMA
//? 2 - I love coding
//? 4 - I love coding
//? 6 - I love coding
//? 8 - I love coding
//? 10 - I love coding
//? 12 - I love coding
//? 14 - I love coding
//? 16 - I love coding
//? 18 - I love coding
//? 20 - I love coding
//? LOOPING KEDUA
//? 20 - I will become a mobile developer
//? 18 - I will become a mobile developer
//? 16 - I will become a mobile developer
//? 14 - I will become a mobile developer
//? 12 - I will become a mobile developer
//? 10 - I will become a mobile developer
//? 8 - I will become a mobile developer
//? 6 - I will become a mobile developer
//? 4 - I will become a mobile developer
//? 2 - I will become a mobile developer

var i = 2;
var text1 = " - I love coding";
var text2 = " - I will become a mobile developer";

console.log('LOOPING PERTAMA')
while (i < 20) {
  console.log(i + text1)
  i += 2;
}
console.log(i + text1)
console.log('LOOPING KEDUA')
while (i >= 2) {
  console.log(i + text2)
  i -= 2;
}

// console.log("LOOPING PERTAMA")
// while (i < 20) {
//   console.log(i + textCoding)
//   i += 2
//   if (i == 20) {
//     console.log(i + textCoding)
//     console.log("LOOPING KEDUA")
//     while (i > 0) {
//       console.log(i + textMobile)
//       i -= 2
//     }
//     break;
//   }
// }

//? Soal 2 - For

//? OUTPUT 
//? 1 - Santai
//? 2 - Berkualitas
//? 3 - I Love Coding 
//? 4 - Berkualitas
//? 5 - Santai
//? 6 - Berkualitas
//? 7 - Santai
//? 8 - Berkualitas
//? 9 - I Love Coding
//? 10 - Berkualitas
//? 11 - Santai
//? 12 - Berkualitas
//? 13 - Santai
//? 14 - Berkualitas
//? 15 - I Love Coding
//? 16 - Berkualitas
//? 17 - Santai
//? 18 - Berkualitas
//? 19 - Santai
//? 20 - Berkualitas

console.log("OUTPUT")
var santai = " - Santai"
var berkualitas = " - Berkualitas"
var loveCoding = " - I Love Coding"
for (i = 1; i <= 20; i++) {
  if (i % 2 != 1) {
    console.log(i + berkualitas)
  } else if (i % 3 == 0) {
    console.log(i + loveCoding)
  } else {
    console.log(i + santai)
  }
}

//? Soal 3 - Persegi Panjang

//? Output:
//? ########
//? ########
//? ########
//? ######## 

i = 1;
var j = 1;
var panjang = 8;
var lebar = 8;
var pagar = '';

while (j <= lebar) { // 9 <= 8
  while (i <= panjang) { // 1 <= 4
    pagar += '#';
    i++;
  }
  console.log(pagar); // ####
  pagar = ''; // ""
  i = 1; // ""
  j++;
}


//? Soal 4 - Segitiga

//? Output:
//? #
//? ##
//? ###
//? ####
//? #####
//? ######
//? #######

// i = 1;
// j = 1;
// var alas = 7;
// var tinggi = 7;
// var pagar = "";

// for (i = 1; i <= tinggi; i++) {
//   for (j = 1; j <= i; j++) {
//     pagar += "#";
//   }
//   console.log(pagar)
//   pagar = "";
// }

//? Soal 5 - Papan Catur

//? Output:
//?  # # # #
//? # # # # 
//?  # # # #
//? # # # # 
//?  # # # #
//? # # # # 
//?  # # # #
//? # # # # 

i = 1;
j = 1;
var panjang = 8;
var lebar = 8;
var papan = "";
for (j = 1; j <= lebar; j++) {
  if (j % 2 == 1) {
    for (i = 1; i <= panjang; i++) {
      if (i % 2 == 1) {
        papan += " "
      } else {
        papan += "#"
      }
    }
  } else {
    for (i = 1; i <= panjang; i++) {
      if (i % 2 == 1) {
        papan += "#"
      } else {
        papan += " "
      }
    }
  }
  console.log(papan);
  papan = "";
}