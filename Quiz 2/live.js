// console.log('pertama')
// setTimeout(() => {
//   console.log('timeout')
// }, 10)
// console.log('terakhir')

const myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Timer Complete')
  }, 1000);
})
  .then((text) => { throw new Error('Failed') })
  .catch(err => console.log(err))
  .then(() => console.log('Execute?'))


  const data = {
    nama: 'Mukhlis'
  }