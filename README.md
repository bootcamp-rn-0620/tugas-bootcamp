# Tugas Bootcamp

## Penjelasan

Berikut adalah kumpulan jawaban dari soal-soal tugas yang diberikan selama bootcamp.
Jawaban ini tidak bersifat mutlak, sehingga peserta dapat menggunakan sintaks-sintaks lain (di luar dari materi yang diberikan) selama peserta dapat memahami materi yang diberikan.

Pastikan setiap soal menghasilkan jawaban seperti yang diharapkan atau dicontohkan pada soal.
Contoh cara memeriksa jawaban adalah dengan menggunakan command node di terminal:

```bash 
  $ node index.js
```

<!-- Berikan perintah di terminal sbb :  
```bash 
  $ npm install
```

## Pengerjaan Challenge
Terdapat folder ```old-javascript``` dimana terdapat beberapa file di dalamnya yaitu index.js, array.js, golden-arrow.js, object-literal.js, dan destructuring.js. File javascript di dalam folder tersebut masih menggunakan sintaks javascript lama (sebelum ES6 atau sebelum ES2015). 
Output yang dihasilkan dari program di folder old-javascript tersebut bisa kamu lihat dengan memberikan perintah script: 

```bash
  $ npm run old-javascript
```

Tugas kamu adalah menuliskan kembali semua program atau file yang ada di dalam old-javascript ke dalam sintaks javascript modern ES6. Pengerjaannya dilakukan di dalam folder es6 yang sudah tersedia (ikuti juga petunjuk dari soal di halaman gitbook)

Untuk mengecek pengerjaanmu, kamu perlu memberikan perintah script: 
```bash
  $ npm run production
```

Output yang dihasilkan dari perintah tersebut harapannya sama dengan apa yang dihasilkan oleh perintah  ``` $ npm run old-javascript``` .

Selamat Mengerjakan! -->
