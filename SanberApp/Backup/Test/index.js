import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {
  SignIn,
  CreateAccount,
  Profile,
  Home,
  Details
} from "./Screens";
import {loginScreen} from "./LoginScreen";
import {aboutScreen} from "./AboutScreen";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const loginStack=createStackNavigator();
const ProfileStack=createStackNavigator();
const MenuStack=createStackNavigator();
const homeStack=createStackNavigator();

//Login
const LoginStackScreen=()=>(
  <loginStack.Navigator>
    <loginStack.Screen name='Form Login' component={loginScreen}/>
  </loginStack.Navigator>
)

//home
const HomeStackScreen=()=>(
  <homeStack.Navigator>
    <homeStack.Screen name='Home' component={Home}/>
    <homeStack.Screen name='Details' component={Details}/>
  </homeStack.Navigator>
)

//Profile
const ProfileStackScreen=()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name='About Me' component={aboutScreen}/>
  </ProfileStack.Navigator>
)

//Menu
const MenuStackScreen=()=>(
  <MenuStack.Navigator>
    <MenuStack.Screen name='Main Menu' component={Home}/>
  </MenuStack.Navigator>
)

export default () => (
  <NavigationContainer>
    <Tabs.Navigator>
      <Tabs.Screen name='Login' component={LoginStackScreen}/>
      <Tabs.Screen name='Profile' component={ProfileStackScreen}/>
      <Tabs.Screen name='Tambah' component={MenuStackScreen}/>

    </Tabs.Navigator>
    {/*<AuthStack.Navigator>
      <AuthStack.Screen
        name="SignIn" options={{title : 'Login'}}
        component={SignIn}
      />
      <AuthStack.Screen
        name="CreateAccount"
        component={CreateAccount}
        options={{title : 'Create Account'}}
      />
    </AuthStack.Navigator>*/}
  </NavigationContainer>
);

