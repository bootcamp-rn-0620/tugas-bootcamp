# IndonesiaMengoding
# InfoQuiz

## Silahkan buat folder baru seperti pada pengerjaan tugas dengan nama Quiz3 di dalam folder project Expo/React Native Anda.
## Masukkan file index.js, LoginScreen.js, HomeScreen.js, data.json ke dalam folder tersebut

## Terdapat 3 Soal dan 2 Soal Bonus

## Setelah selesai mengerjakan, push pekerjaan Anda ke repository gitlab masing-masing
## Input link commit hasil pekerjaan Anda ke sanbercode.com

## Kerjakan soal semampunya dengan baik dan jujur
## Tidak perlu bertanya kepada trainer terkait soal, cukup dikerjakan sesuai pemahaman peserta
## Peserta diperbolehkan untuk googling, namun tidak diperbolehkan untuk bertanya, berdiskusi, atau mencontek.
## Selama quiz berlangsung, grup diskusi telegram akan dinonaktifkan (peserta tidak dapat mengirimkan pesan atau berdiskusi di grup)

# Selamat mengerjakan