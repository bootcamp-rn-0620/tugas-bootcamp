import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Kotak from "./Latihan/Component/Component";

export default function App() {
  return (
    <Kotak />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start </Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
